#!/bin/bash

source ./fuego-ro/conf/fuego.conf

readonly internal_fuego_port=${jenkins_port:-8080}
readonly fuego_rt_image=fuego-rt
readonly fuego_rt_container=fuego-rt-container
detached_mode_opts="--interactive --attach"
port_opts="-p 8080:${internal_fuego_port}"

usage() {
    echo "usage: $0 [OPTIONS] up|down"
    grep " .)\\ #" "${0}"
    exit 0
}

[ $# -eq 0 ] && usage
while getopts "hdcr:b:p:" arg; do
  case $arg in
    d) # Run in detached mode
      detached_mode_opts=""
      ;;
    c) # Start from scratch, rebuilding image
      clean_start="true"
      ;;
    r) # Fuego Repository to be tested (default: Fuego Official Repo)
      fuego_repo_opts="-e TEST_REPO=${OPTARG}"
      ;;
    b) # Fuego Branch to be tested (default: Master)
      fuego_branch_opts="-e TEST_BRANCH=${OPTARG}"
      ;;
    p) # Fuego (the stable, not the one under test) port mapped on Host (default: 8090)
      port_opts="-p ${OPTARG}:${internal_fuego_port}"
      ;;
    h | *) # Display help.
      usage
      exit 0
      ;;
  esac
done
shift "$((OPTIND - 1))"

if [ "${1}" != "up" ] && [ "${1}" != "down" ]; then
    usage
fi

if [ "$1" = "down" ]; then
    docker stop ${fuego_rt_container}
fi

if [ "$1" = "up" ]; then
    if [ -v clean_start ]; then
        docker build -t "${fuego_rt_image}" .
        docker rm -f "${fuego_rt_container}"
        docker run -dit --name ${fuego_rt_container} \
            -v /var/run/docker.sock:/var/run/docker.sock \
            --net=bridge \
            ${port_opts} ${fuego_rt_image}
       docker exec \
           ${fuego_repo_opts} \
           ${fuego_branch_opts} \
           -e FUEGO_PORT="${internal_fuego_port}" \
           ${fuego_rt_container} /fuego-rt-entrypoint.sh
       docker stop ${fuego_rt_container}
    fi
    docker start ${detached_mode_opts} ${fuego_rt_container}
fi
