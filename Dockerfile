from fuegotest/fuego-base:latest

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get -yV install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg2 \
        python3 \
        python3-pip \
        software-properties-common && \
    curl -fsSL \
        https://download.docker.com/linux/$(\
            source /etc/os-release; echo "$ID")/gpg \
        | sudo apt-key add - && \
    add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/$(\
            source /etc/os-release; echo "$ID") \
            $(lsb_release -cs) stable" && \
    apt-get update && \
    apt-get -yV install \
        docker-ce && \
    pip3 install pexpect

RUN echo "jenkins ALL = (root) NOPASSWD: /usr/bin/docker, /bin/sh" >> /etc/sudoers

COPY fuego-ro /fuego-ro
COPY tests /fuego-core/engine/tests
COPY fuego-rt-entrypoint.sh /
