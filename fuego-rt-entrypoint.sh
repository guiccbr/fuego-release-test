#!/bin/bash

set -e

function wait_for_fuego {
    readonly fuego_port=${FUEGO_PORT:-8080}
    readonly jenkins_url="http://localhost:${fuego_port}/fuego/"

    echo "Waiting for Jenkins on ${jenkins_url}..."
    while ! curl --output /dev/null --silent --head --fail "${jenkins_url}";
    do
        sleep 1
    done
}

test_repo=${TEST_REPO:-https://bitbucket.org/profusionmobi/fuego}
test_repo_escaped=$(sed 's/[&/\]/\\&/g' <<< "${test_repo}")
sed -i "s/FUEGOTEST_REPO/${test_repo_escaped}/" /fuego-core/engine/tests/Functional.fuegotest/spec.json

test_branch=${TEST_BRANCH:-"fuego-base"}
test_branch_escaped=$(sed 's/[&/\]/\\&/g' <<< "${test_branch}")
sed -i "s/FUEGOTEST_BRANCH/${test_branch_escaped}/" /fuego-core/engine/tests/Functional.fuegotest/spec.json

wait_for_fuego
ftc add-nodes fuego-test
ftc add-jobs -b fuego-test -t Functional.fuegotest
