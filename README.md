# Fuego Release Test

This Fuego Image has all the tools needed for the automation of the tests that
are required for validating a new Fuego release with Fuego itself.

It consists basically of:
* A Dockerfile containing the installation of additional dependencies.
* A new fuego-test board (fuego-ro/boards/fuego-test)
* A functional test (tests/Functional.fuegotest) that will download a specific
  branch of Fuego from a repository provided by the user, start an inner
  container running that specific fuego release and run basic tests on it.

## Build and Run Instructions

In order to build and run the tests with minimal effort, just use the given
`build_and_run.sh` script with the following parameters:

```
$ build_and_run.sh -r ${fuego-repo:-FUEGO_OFFICIAL_REPO} -b ${branch:-MASTER} -p ${fuego_port} up
```

In order to stop and save your session simply execute.

```
$ build_and_run.sh down
```

Please see `$ build_and_run.sh -h` for more options.


## Additional Instructions

By running fuego-release-test, as explained above, you should be able to access
fuego through `http://localhost:${fuego_port}/fuego/`

There you will see that the fuego-test board is already available and that the
default test has been added to it. Simply use Fuego's Jenkins interface as
usual to test Fuego itself.
