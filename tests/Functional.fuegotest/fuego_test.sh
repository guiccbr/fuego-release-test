#!/bin/bash

set -e

readonly fuego_release_dir=fuego-release
readonly fuego_release_img_name=fuego-release
readonly fuego_release_container_name=fuego-release-container

function test_build {
    if [ -d ${fuego_release_dir} ]; then
        rm -r ${fuego_release_dir}
    fi
    git clone --quiet --depth 1 --single-branch \
        --branch "${FUNCTIONAL_FUEGOTEST_BRANCH}" \
        "${FUNCTIONAL_FUEGOTEST_REPO}" \
        "${fuego_release_dir}"
    cd "${fuego_release_dir}"
    git submodule update --init --recursive
    cd -
}

function test_run {
    cd "${fuego_release_dir}"
    ./install.sh "${fuego_release_img_name}"
    sudo -n docker rm -f "${fuego_release_container_name}" || true
    sudo -n docker create -it --name "${fuego_release_container_name}" \
        -p 8081:8080 \
        --net="bridge" "${fuego_release_img_name}"

    python3 ${TEST_HOME}/test_run.py
    report "echo ok 1 minimal test on target"
}

function test_processing {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
