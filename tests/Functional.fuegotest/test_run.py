#!/usr/bin/env python

import pexpect

# TODO: Check for errors

c = pexpect.spawnu('sudo docker start --interactive --attach fuego-release-container')
c.sendline('\r\n')
c.expect('# $')

print('Adding Node')
c.sendline('ftc add-nodes docker')
c.expect('# $', timeout=120)

print('Adding Jobs')
c.sendline('ftc add-jobs -b docker -p testplan_docker')
c.expect('# $', timeout=120)

print('Node and Jobs have been added')
